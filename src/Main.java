import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
	
	
	JFrame frame;
	JPanel window;
	private Main(){
		window = new CartesianPanel();
		frame = new JFrame();
		
		frame.add(window);
		frame.setVisible(true);
		frame.setSize(600,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String[] args){
		Main m = new Main();
	}
}
